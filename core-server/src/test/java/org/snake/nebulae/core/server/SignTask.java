package org.snake.nebulae.core.server;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.Sign;
import cn.hutool.crypto.asymmetric.SignAlgorithm;
import lombok.extern.slf4j.Slf4j;

import java.util.OptionalInt;
import java.util.stream.IntStream;

@Slf4j
public class SignTask implements Runnable {

    @Override
    public void run() {
        int loopTimes = RandomUtil.randomInt(99, 999);
        int signRightCount = 0;

        OptionalInt max = IntStream.range(0, loopTimes).map(operand -> {
            String data = RandomUtil.randomString(8);

            Sign sign = SecureUtil.sign(SignAlgorithm.MD5withRSA);

            byte[] signData = sign.sign(data.getBytes());
            boolean verify = sign.verify(data.getBytes(), signData);

            if (verify) {
                return RandomUtil.randomInt(99, 999);
            }

            return RandomUtil.randomInt(99, 999);
        }).max();

        // log.info("我是 {} 我的名字 {} 时间是 {}", this.getClass(), name, DateUtil.now());
    }
}
