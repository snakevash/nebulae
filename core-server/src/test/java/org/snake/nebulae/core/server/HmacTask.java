package org.snake.nebulae.core.server;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.digest.HMac;
import cn.hutool.crypto.digest.HmacAlgorithm;
import lombok.extern.slf4j.Slf4j;

import java.util.OptionalInt;
import java.util.stream.IntStream;

@Slf4j
public class HmacTask implements Runnable {

    @Override
    public void run() {
        int loopTimes = RandomUtil.randomInt(99, 999);
        int signRightCount = 0;

        OptionalInt max = IntStream.range(0, loopTimes).map(operand -> {
            String data = RandomUtil.randomString(8);

            String key = RandomUtil.randomString(10);
            HMac hMac = new HMac(HmacAlgorithm.HmacMD5, key.getBytes());

            String digestHex = hMac.digestHex(data);

            return RandomUtil.randomInt(digestHex.length(), 999);
        }).max();
    }
}
