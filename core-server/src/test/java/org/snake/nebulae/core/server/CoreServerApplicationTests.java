package org.snake.nebulae.core.server;

import org.junit.jupiter.api.Test;
import org.snake.nebulae.core.service.TaskExecutorManager;
import org.snake.nebulae.core.service.TaskExecutorReportTask;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = CoreServerApplication.class)
class CoreServerApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    public void taskExecutorManager() throws InterruptedException {
        TaskExecutorManager taskExecutorManager = new TaskExecutorManager();

        // 每500毫秒执行一次任务生成机制
        taskExecutorManager.scheduleWithFixedDelay(new GeneratorTask(taskExecutorManager), 500);
        // 每5秒执行一次任务信息采集
        taskExecutorManager.scheduleWithFixedDelay(new TaskExecutorReportTask(taskExecutorManager), 5000);

        Thread.sleep(1000 * 60 * 10);
    }

    @Test
    public void computeTask() {
        new ComputeTask().run();
    }
}
