package org.snake.nebulae.core.server;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import lombok.extern.slf4j.Slf4j;

import java.util.OptionalInt;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

@Slf4j
public class AesTask implements Runnable {

    @Override
    public void run() {
        int loopTimes = RandomUtil.randomInt(99, 999);

        OptionalInt max = IntStream.range(0, loopTimes).map((IntUnaryOperator) operand -> {
            String data = RandomUtil.randomString(8);

            byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();

            SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);

            byte[] encrypt = aes.encrypt(data);
            byte[] decrypt = aes.decrypt(encrypt);

            String encryptHex = aes.encryptHex(data);
            String decryptStr = aes.decryptStr(encryptHex, CharsetUtil.CHARSET_UTF_8);

            if (decryptStr.equals(data)) {
                return RandomUtil.randomInt(99, 999);
            }
            return RandomUtil.randomInt(99, 999);
        }).max();

        // log.info("我是 {} 我会说 {} 时间是 {}", this.getClass(), say, DateUtil.now());
    }
}
