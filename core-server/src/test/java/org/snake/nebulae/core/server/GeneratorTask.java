package org.snake.nebulae.core.server;

import cn.hutool.core.util.RandomUtil;
import lombok.SneakyThrows;
import org.snake.nebulae.core.service.TaskExecutorManager;
import org.snake.nebulae.core.service.TaskWrap;

import java.util.ArrayList;
import java.util.List;

public class GeneratorTask implements Runnable {

    private List<Class<?>> classes = new ArrayList<>();

    private TaskExecutorManager taskExecutorManager;

    public GeneratorTask(TaskExecutorManager taskExecutorManager) {
        this.taskExecutorManager = taskExecutorManager;

        classes.add(SignTask.class);
        classes.add(ComputeTask.class);
        classes.add(AesTask.class);
        classes.add(HmacTask.class);
    }

    @SneakyThrows
    @Override
    public void run() {
        int loopTimes = RandomUtil.randomInt(9, 99);
        for (int i = 0; i < loopTimes; i++) {
            int index = RandomUtil.randomInt(0, classes.size());
            Class<?> klass = classes.get(index);

            taskExecutorManager.submit(new TaskWrap(((Runnable) klass.newInstance())));
        }
    }
}
