package org.snake.nebulae.core.server;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.digest.MD5;
import lombok.extern.slf4j.Slf4j;

import java.util.OptionalLong;
import java.util.stream.LongStream;

@Slf4j
public class ComputeTask implements Runnable {

    @Override
    public void run() {
        byte[] bytes = RandomUtil.randomBytes(6);
        int loopTimes = RandomUtil.randomInt(555, 9999);

        long loopTimeStart = System.currentTimeMillis();
        OptionalLong max = LongStream.rangeClosed(1, loopTimes).map(operand -> {
            String s = MD5.create().digestHex(bytes);
            String t = MD5.create().digestHex(s.getBytes());
            String u = MD5.create().digestHex(t.getBytes());
            
            return System.currentTimeMillis();
        }).max();
        // log.info("计算任务完成 循环次数: {} 最大值是: {} 消耗时间: {}", loopTimes, max.orElseGet(() -> 0), (System.currentTimeMillis() - loopTimeStart));
    }
}
