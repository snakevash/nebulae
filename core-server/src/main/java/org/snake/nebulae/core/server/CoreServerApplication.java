package org.snake.nebulae.core.server;

import org.snake.nebulae.core.task.ClearSercieTask;
import org.snake.nebulae.core.task.CoreTask;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import javax.annotation.Resource;

@SpringBootApplication(scanBasePackageClasses = CoreServerApplication.class, scanBasePackages = "org.snake.nebulae.core")
public class CoreServerApplication implements CommandLineRunner {

    @Resource
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    @Resource
    private CoreTask coreTask;

    private ClearSercieTask clearSercieTask;

    public static void main(String[] args) {
        SpringApplication.run(CoreServerApplication.class, args);
    }

    @Override
    public void run(String... args) {
        threadPoolTaskScheduler.submit(coreTask);

        this.clearSercieTask = new ClearSercieTask(coreTask.getDoRegisteredPlugins(), threadPoolTaskScheduler);
    }

}
