package org.snake.nebulae.emulation;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import javax.annotation.Resource;

@SpringBootApplication(scanBasePackages = "org.snake.nebulae.core", scanBasePackageClasses = EmulationServerApplication.class)
public class EmulationServerApplication implements CommandLineRunner {

    @Resource
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    @Resource
    private FruitsServiceTask fruitsServiceTask;

    public static void main(String[] args) {
        SpringApplication.run(EmulationServerApplication.class, args);
    }

    @Override
    public void run(String... args) {
        // 程序入口
        // 每个服务由一个服务的mainTask来构造

        threadPoolTaskScheduler.submit(fruitsServiceTask);
    }
}
