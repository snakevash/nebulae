package org.snake.nebulae.emulation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.snake.nebulae.core.model.RequestInfo;
import org.snake.nebulae.core.model.ResponseInfo;
import org.snake.nebulae.core.model.ServiceHandleAdapter;
import org.snake.nebulae.core.model.ServiceInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

// todo 所有配置都改成注解配置
// todo 反射字节码生成相关handle
@Slf4j
public class FruitsServiceHandle extends ServiceHandleAdapter {

    private Map<String, Double> fruits = new ConcurrentHashMap<>();

    public FruitsServiceHandle(MqttAsyncClient mqttAsyncClient, ServiceInfo serviceInfo, ObjectMapper objectMapper) {
        super(mqttAsyncClient, serviceInfo, objectMapper);

        fruits.put("apple", 10.5);
        fruits.put("banana", 11.5);
        fruits.put("peach", 12.6);
        fruits.put("watermelon", 13.1);
    }

    @Override
    public void doInit() {
        name = "fruitService";
        methodName = "getPrice";

        params.add("name");
    }

    @Override
    public Object doLogic(RequestInfo requestInfo) {
        ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setRequestInfo(requestInfo);

        if (fruits.containsKey(requestInfo.getParams().get("name"))) {
            List<Object> r = new ArrayList<>();
            r.add(fruits.get(requestInfo.getParams().get("name")));
            responseInfo.setData(r);
        }

        responseInfo.setCode(200);
        return responseInfo;
    }
}
