package org.snake.nebulae.emulation.client;

import lombok.extern.slf4j.Slf4j;
import org.snake.nebulae.core.model.RequestInfo;
import org.snake.nebulae.core.model.ResponseInfo;
import org.snake.nebulae.core.task.CoreRequestTask;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Component
@Slf4j
public class FruitsRequestTask extends CoreRequestTask {

    public CompletableFuture<ResponseInfo> doResponse = new CompletableFuture<>();

    @Override
    protected void doSetRequestInfo(RequestInfo requestInfo) {
        super.doSetRequestInfo(requestInfo);

        requestInfo.setName("fruitService");
        requestInfo.setMethodName("getPrice");

        Map<String, String> params = new HashMap<>();
        params.put("name", "apple");
        requestInfo.setParams(params);
    }

    @Override
    protected void doLogic() {

        end.thenAcceptAsync(iMqttToken -> {
            log.info("结果内容是 {} 价格 {}", "apple", this.responseInfo.getData().get(0));

            doResponse.complete(this.responseInfo);
        });
    }
}
