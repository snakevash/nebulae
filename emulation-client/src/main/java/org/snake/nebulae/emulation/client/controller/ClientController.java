package org.snake.nebulae.emulation.client.controller;

import org.snake.nebulae.core.model.ResponseInfo;
import org.snake.nebulae.emulation.client.FruitsRequestTask;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@RestController
public class ClientController {

    @Resource
    private FruitsRequestTask fruitsRequestTask;

    @Resource
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    @GetMapping("/client/version")
    public Mono<String> index() {
        return Mono.just("fruits 0.0.1");
    }

    @GetMapping("/client/fruits")
    public Mono<ResponseInfo> fruits() {
        threadPoolTaskScheduler.submit(fruitsRequestTask);

        return Mono.fromCompletionStage(fruitsRequestTask.doResponse);
    }
}
