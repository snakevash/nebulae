package org.snake.nebulae.emulation.client;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "org.snake.nebulae.core", scanBasePackageClasses = EmulationClientApplication.class)
public class EmulationClientApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(EmulationClientApplication.class, args);
    }

    @Override
    public void run(String... args) {
        // 客户端入口
    }
}
