package org.snake.nebulae.core.controller;

import org.snake.nebulae.core.model.MqttResponse;
import org.snake.nebulae.core.service.MetaInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@RestController
public class IndexController {

    @Resource
    private MetaInfoService metaInfoService;

    @GetMapping("/index")
    public Mono<String> index() {
        return Mono.just("index");
    }

    @GetMapping("/index/subscriptions")
    public Mono<? extends MqttResponse> subscriptionInfo() {
        return metaInfoService.recordSubscriptions();
    }
}
