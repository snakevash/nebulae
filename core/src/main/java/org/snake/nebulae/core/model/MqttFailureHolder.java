package org.snake.nebulae.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.eclipse.paho.client.mqttv3.IMqttToken;

@Data
@AllArgsConstructor
public class MqttFailureHolder {

    private IMqttToken asyncActionToken;

    private Throwable exception;
}
