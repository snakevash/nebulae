package org.snake.nebulae.core.service;

import cn.hutool.core.lang.UUID;
import lombok.Data;

import java.util.concurrent.Callable;

@Data
public class TaskWrap {

    /**
     * 每个任务都有一个id
     */
    private String uuid = UUID.fastUUID().toString();

    private Runnable runnable;

    private Callable<?> callable;

    /**
     * 任务本身 class
     */
    private Class<?> klass;

    /**
     * 任务执行开始时间
     */
    private long taskExecutorStartTime;

    /**
     * 任务执行结束时间
     */
    private long taskExecutorEndTime;

    /**
     * 任务执行消耗时间
     */
    private long taskExecutorTime;

    public TaskWrap(Runnable runnable) {
        this.runnable = runnable;

        this.klass = runnable.getClass();
    }

    public TaskWrap(Callable<?> callable) {
        this.callable = callable;

        this.klass = callable.getClass();
    }

    public void recordTime() {
        this.taskExecutorTime = this.taskExecutorEndTime - this.taskExecutorStartTime;
    }
}
