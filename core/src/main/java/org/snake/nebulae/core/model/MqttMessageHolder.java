package org.snake.nebulae.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.eclipse.paho.client.mqttv3.MqttMessage;

@Data
@AllArgsConstructor
public class MqttMessageHolder {

    private String topic;

    private MqttMessage mqttMessage;
}
