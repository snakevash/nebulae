package org.snake.nebulae.core.service;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TaskExecutorReport {

    /**
     * 报告时间点
     */
    private long reportTime;

    /**
     * 报告时间点 可读
     */
    private String reportTimeStr;

    /**
     * 任务类型数量
     */
    private int taskTypeSum;

    /**
     * 平均所有任务完成的平均时间
     */
    private double executorTimeAvg;

    /**
     * 任务列表
     */
    private List<TaskReport> reports = new ArrayList<>();
}
