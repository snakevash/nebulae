package org.snake.nebulae.core.model;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class RequestInfo {

    /**
     * 请求id
     * 用于唯一标志请求
     * 为后续逻辑提供唯一性保证
     */
    private String requestId;

    /**
     * 请求服务名称
     */
    private String name;

    /**
     * 请求方法名称
     */
    private String methodName;

    /**
     * 请求参数列表
     */
    private Map<String, String> params = new HashMap<>();

    /**
     * 请求主题
     */
    private String requestTopic;

    /**
     * 接受结果主题
     */
    private String responseTopic;
}
