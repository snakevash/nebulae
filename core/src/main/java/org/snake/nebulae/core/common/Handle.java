package org.snake.nebulae.core.common;

import org.eclipse.paho.client.mqttv3.MqttMessage;

public interface Handle {
    public void handleRquest(String topic, MqttMessage message);
}
