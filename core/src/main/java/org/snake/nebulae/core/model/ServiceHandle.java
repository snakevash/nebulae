package org.snake.nebulae.core.model;

import java.util.ArrayList;
import java.util.List;

abstract class ServiceHandle {

    /**
     * 服务名称
     */
    protected String name;

    /**
     * 方法名称
     */
    protected String methodName;

    /**
     * 参数
     */
    protected List<Object> params = new ArrayList<>();

    public String getName() {
        return name;
    }

    public String getMethodName() {
        return methodName;
    }

    /**
     * 处理逻辑入口
     *
     * @param requestInfo 请求信息
     */
    public void handle(RequestInfo requestInfo) {
        // 检查参数
        if (!doCheckParams(requestInfo)) {
            throw new Error("参数校验失败");
        }
        // 处理业务
        Object result = doLogic(requestInfo);
        // 返回结果
        doReturn(result);
    }

    /**
     * 初始化配置
     */
    public abstract void doInit();

    /**
     * 检查参数有效性
     *
     * @param requestInfo 请求信息
     * @return 布尔
     */
    public abstract boolean doCheckParams(RequestInfo requestInfo);

    /**
     * 业务处理需要重写的方法
     *
     * @param requestInfo 请求信息
     * @return 结果信息
     */
    public abstract Object doLogic(RequestInfo requestInfo);

    /**
     * 返回业务逻辑
     *
     * @param result 结果
     */
    public abstract void doReturn(Object result);
}
