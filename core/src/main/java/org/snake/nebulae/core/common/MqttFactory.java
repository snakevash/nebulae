package org.snake.nebulae.core.common;

import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

public interface MqttFactory {

    /**
     * 自定义链接配置
     *
     * @return mqttConnectOptions
     */
    MqttConnectOptions getCustomMqttConnectionOptions();

    /**
     * 自定义mqtt异步客户端
     *
     * @return mqttAsyncClient
     */
    MqttAsyncClient getCustomMqttAsyncClient() throws MqttException;

    /**
     * 选择一个客户端
     *
     * @return mqttAsyncClient
     */
    MqttAsyncClient choseOneClient();
}
