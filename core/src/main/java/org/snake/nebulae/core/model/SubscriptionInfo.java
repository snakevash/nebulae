package org.snake.nebulae.core.model;

import lombok.Data;

@Data
public class SubscriptionInfo {

    private String topic;

    private int qos;

    private String node;

    private String clientid;
}
