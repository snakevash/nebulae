package org.snake.nebulae.core.model;

import lombok.Data;

@Data
public class MqttResponseMetaInfo {

    private int page;

    private int limit;

    private boolean hasnext;

    private int count;
}
