package org.snake.nebulae.core.exception;

public class ResponseException extends Exception {

    public ResponseException(String message) {
        super(message);
    }
}
