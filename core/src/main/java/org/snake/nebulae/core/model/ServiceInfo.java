package org.snake.nebulae.core.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ServiceInfo {

    /**
     * 服务隶属于哪个服务模块
     */
    private String name;

    /**
     * 服务所在主题地址
     */
    private String servicePath;

    /**
     * 服务id
     * 每个服务都有自身标签用于区分不同服务之星过来的结果
     */
    private String serviceId;

    /**
     * 注册时间
     */
    private long registerTime;

    /**
     * 最后一次心跳时间信息
     */
    private long lastHeartBeatTime;

    /**
     * 服务所拥有的方法名称
     */
    public List<String> methods = new ArrayList<>();
}
