package org.snake.nebulae.core.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.snake.nebulae.core.model.RequestInfo;
import org.snake.nebulae.core.model.ServiceHandleAdapter;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Slf4j
public class DispatcherHandle {

    @Resource
    private ObjectMapper objectMapper;

    /**
     * 方法处理器列表
     */
    private ConcurrentHashMap<String, ConcurrentHashMap<String, ServiceHandleAdapter>> registerHandles = new ConcurrentHashMap<>();

    public ConcurrentHashMap<String, ConcurrentHashMap<String, ServiceHandleAdapter>> getRegisterHandles() {
        return registerHandles;
    }

    public void register(ServiceHandleAdapter serviceHandleAdapter) {
        if (!registerHandles.containsKey(serviceHandleAdapter.getName())) {
            ConcurrentHashMap<String, ServiceHandleAdapter> tmp = new ConcurrentHashMap<>();
            tmp.put(serviceHandleAdapter.getMethodName(), serviceHandleAdapter);
            registerHandles.put(serviceHandleAdapter.getName(), tmp);
        } else {
            if (!registerHandles.get(serviceHandleAdapter.getName()).containsKey(serviceHandleAdapter.getMethodName())) {
                registerHandles.get(serviceHandleAdapter.getName()).put(serviceHandleAdapter.getMethodName(), serviceHandleAdapter);
            } else {
                log.error("已经注册过相关模块和方法 {} {}", serviceHandleAdapter.getName(), serviceHandleAdapter.getMethodName());
            }
        }
    }

    public void register(String topic, MqttMessage message) {
        try {
            String msg = new String(message.getPayload());
            log.info("收到主题 {} 的消息内容: {}", topic, msg);

            ServiceHandleAdapter serviceHandle = objectMapper.readValue(msg, new TypeReference<ServiceHandleAdapter>() {
            });

            if (!registerHandles.containsKey(serviceHandle.getName())) {
                ConcurrentHashMap<String, ServiceHandleAdapter> tmp = new ConcurrentHashMap<>();
                registerHandles.put(serviceHandle.getName(), tmp);
            } else {
                if (!registerHandles.get(serviceHandle.getName()).containsKey(serviceHandle.getMethodName())) {
                    registerHandles.get(serviceHandle.getName()).put(serviceHandle.getMethodName(), serviceHandle);
                } else {
                    log.error("已经注册过相关模块和方法 {} {}", serviceHandle.getName(), serviceHandle.getMethodName());
                }
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public void request(String topic, MqttMessage message) {
        try {
            String msg = new String(message.getPayload());
            log.info("收到主题 {} 的消息内容: {}", topic, msg);

            if (registerHandles.size() == 0) {
                log.error("方法处理器列表未包含任何处理逻辑: {}", topic);
                return;
            }

            RequestInfo requestInfo = objectMapper.readValue(msg, RequestInfo.class);

            if (!registerHandles.containsKey(requestInfo.getName())) {
                log.error("没有找到相关的服务模块");
                return;
            }

            if (!registerHandles.get(requestInfo.getName()).containsKey(requestInfo.getMethodName())) {
                log.error("没有找到相关的服务功能");
                return;
            }

            // 处理逻辑入口
            registerHandles.get(requestInfo.getName()).get(requestInfo.getMethodName()).handle(requestInfo);

        } catch (JsonProcessingException e) {
            log.error("解析json格式出错 {}", e.getMessage());
        } catch (Exception e) {
            log.error("发生致命异常 需要处理 {}", e.getMessage());
        }
    }
}
