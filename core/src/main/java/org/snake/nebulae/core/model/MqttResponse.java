package org.snake.nebulae.core.model;

import lombok.Data;

import java.util.List;

@Data
public class MqttResponse<T> {

    private List<T> data;

    private MqttResponseMetaInfo meta;

    private int code;
}
