package org.snake.nebulae.core.service;

import lombok.Data;

@Data
public class TaskReport {

    /**
     * 任务类型
     */
    private Class<?> klass;

    /**
     * 报告时间点
     */
    private long reportTime;

    /**
     * 报告时间点 可读
     */
    private String reportTimeStr;

    /**
     * 完成任务数量
     */
    private int taskSum;

    /**
     * 执行任务平均时间
     */
    private double executorTimeAvg;

    /**
     * 变化趋势
     * + 上升
     * = 平稳
     * - 下降
     */
    private String trend;
}
