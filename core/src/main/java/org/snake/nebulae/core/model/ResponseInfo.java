package org.snake.nebulae.core.model;

import lombok.Data;

import java.util.List;

@Data
public class ResponseInfo {

    private RequestInfo requestInfo;

    private int code;

    private List<Object> data;
}
