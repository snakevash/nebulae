package org.snake.nebulae.core.task;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.snake.nebulae.core.common.DispatcherHandle;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Slf4j
public class MessageMainTask implements Runnable {

    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    private String topic;

    private MqttMessage mqttMessage;

    private DispatcherHandle dispatcherHandle;

    public MessageMainTask(ThreadPoolTaskScheduler threadPoolTaskScheduler, DispatcherHandle dispatcherHandle, String topic, MqttMessage mqttMessage) {
        this.threadPoolTaskScheduler = threadPoolTaskScheduler;
        this.dispatcherHandle = dispatcherHandle;
        this.topic = topic;
        this.mqttMessage = mqttMessage;
    }

    @Override
    public void run() {
        dispatcherHandle.request(topic, mqttMessage);
    }
}
